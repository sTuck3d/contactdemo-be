package com.example.demo;

import com.example.demo.db.ContactRepository;
import com.example.demo.db.entity.Contact;
import com.example.demo.exception.ContactNotExsitingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
public class ContactServiceTest {

    @Mock
    private ContactRepository repository;

    @InjectMocks
    private ContactService underTest;

    Contact contact1 = new Contact("first1","last1","test1@google.de");
    Contact contact2 = new Contact("first2","last2","test2@google.de");
    Contact contact3 = new Contact("first3","last3","test3@google.de");

    @BeforeEach
    public void init(){
        List<Contact> testContacts = Arrays.asList(contact1, contact2, contact3);
        when(repository.findAll()).thenReturn(testContacts);
        when(repository.existsById("test1@google.de")).thenReturn(true);
        when(repository.existsById("notexistingMail")).thenReturn(false);
        when(repository.findById("test1@google.de")).thenReturn(Optional.ofNullable(contact1));
    }

    @Test
    public void assert_getAllContacts_works(){
        List<Contact> contacts = underTest.getAllContacts();
        contacts.containsAll(Arrays.asList(contact1, contact2, contact3));
    }

    @Test
    public void assert_gettingContact_works(){
        try {
            Optional<Contact> contact = underTest.getContact("test1@google.de");
            assertTrue(contact.isPresent());
            assertEquals(contact1, contact.get());
        } catch (ContactNotExsitingException e) {
            fail();
        }
    }

    @Test
    public void assert_gettingUnknownAddress_ThrowsException(){
        try {
            Optional<Contact> contact = underTest.getContact("notexistingMail");
        } catch (ContactNotExsitingException e) {
            return;
        }
        fail();
    }
}
