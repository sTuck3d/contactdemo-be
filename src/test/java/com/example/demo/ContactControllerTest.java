package com.example.demo;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureMockMvc
public class ContactControllerTest {

    @Mock
    private ContactService service;

    @Test
    public void assert_newContact_works() throws Exception {
        // TODO test with mockMVC and maybe use testdata in database with liquibase
    }

    @Test
    public void assert_newContact_twice_throwsException(){

    }

}
