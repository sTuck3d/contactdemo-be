package com.example.demo.exception;

public class ContactNotExsitingException extends Exception{

    public ContactNotExsitingException(String message){
        super(message);
    }

}
