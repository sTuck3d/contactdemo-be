package com.example.demo.exception;

public class ContactAlreadyExisitsException extends Exception {

    public ContactAlreadyExisitsException(String message){
        super(message);
    }

}
