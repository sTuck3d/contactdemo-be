package com.example.demo;

import com.example.demo.exception.ContactAlreadyExisitsException;
import com.example.demo.exception.ContactNotExsitingException;
import com.example.demo.db.entity.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class ContactController {

    private final String CONTACT_NOT_EXISTING_MESSAGE = "Contact not in database";
    private final String CONTACT_ALREADY_EXISTING_MESSAGE = "Contact already exists";

    @Autowired
    private ContactService contactService;

    @GetMapping("/contacts")
    public List<Contact> getAllContacts(){
        return contactService.getAllContacts();
    }

    @GetMapping("/contact/{address}")
    public Optional<Contact> getContact(@PathVariable String address){
        try {
            return contactService.getContact(address);
        } catch (ContactNotExsitingException e) {
            throw createNewBadRequestException(e,CONTACT_NOT_EXISTING_MESSAGE);
        }
    }

    @PostMapping("/contacts/purge")
    public void purgeDatabase(){
        contactService.purgeDatabase();
    }

    @PostMapping("/contact/new")
    public void insertNewContact(@Valid @RequestBody Contact contact){
        try {
            contactService.insertNewContact(contact);
        } catch (ContactAlreadyExisitsException e) {
            throw createNewBadRequestException(e, CONTACT_ALREADY_EXISTING_MESSAGE);
        }
    }

    @PutMapping("/contact/change")
    public Contact changeContact(@Valid @RequestBody Contact contact){
        Contact changedContact;
        try {
            changedContact = contactService.change(contact);
        } catch (ContactNotExsitingException e) {
            throw createNewBadRequestException(e, CONTACT_NOT_EXISTING_MESSAGE);
        }
        return changedContact;
    }

    @PutMapping("/contact/delete/{address}")
    public void deleteContact(@PathVariable String address){
        try {
            contactService.delete(address);
        } catch (ContactNotExsitingException e) {
            throw createNewBadRequestException(e, CONTACT_NOT_EXISTING_MESSAGE);
        }
    }

    private ResponseStatusException createNewBadRequestException(Exception e, String exceptionReason){
        return new ResponseStatusException(
                HttpStatus.BAD_REQUEST, exceptionReason, e);
    }
}
