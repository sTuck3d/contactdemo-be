package com.example.demo;

import com.example.demo.exception.ContactAlreadyExisitsException;
import com.example.demo.exception.ContactNotExsitingException;
import com.example.demo.db.ContactRepository;
import com.example.demo.db.entity.Contact;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ContactService {

    private final Logger LOGGER = LoggerFactory.getLogger(ContactController.class);

    @Autowired
    private ContactRepository repository;

    public List<Contact> getAllContacts(){
        return repository.findAll();
    }

    public Optional<Contact> getContact(String address) throws ContactNotExsitingException{
        throwExceptionIfIdIsNotInRepository(address);
        return repository.findById(address);
    }

    public void purgeDatabase(){
        repository.deleteAll();
    }

    public void insertNewContact(Contact contact) throws ContactAlreadyExisitsException {
        if(repository.existsById(contact.getAddress())) throw new ContactAlreadyExisitsException("Contact already exists: " + contact.getAddress());
        repository.insert(contact);
        LOGGER.info("New Contact saved: " + contact);
    }

    public Contact change(Contact contact) throws ContactNotExsitingException {
        throwExceptionIfIdIsNotInRepository(contact.getAddress());
        LOGGER.info("Changed contact: " + contact.getAddress());
        return repository.save(contact);
    }

    public void delete(String address) throws ContactNotExsitingException {
        throwExceptionIfIdIsNotInRepository(address);
        repository.deleteById(address);
        LOGGER.info("Contact deleted: " + address);
    }

    private void throwExceptionIfIdIsNotInRepository(String address) throws ContactNotExsitingException {
        if(!repository.existsById(address)) throw new ContactNotExsitingException("Contact does not exist: " + address);
    }
}
