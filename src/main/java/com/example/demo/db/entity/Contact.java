package com.example.demo.db.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class Contact {

    @NotNull
    @Size(min= 3, max=15)
    private String firstName;

    @NotNull
    @Size(min= 3, max=15)
    private String lastName;

    @NotNull
    @Id
    @Email
    private String address;

}
